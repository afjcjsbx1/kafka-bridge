ROOT_PATH=$(pwd)
echo "ROOT_PATH: $ROOT_PATH"

FLASH_PATH="$ROOT_PATH/../glin-bp00249-lan-boflashfaultmgmtstm"
echo "FLASH_PATH: $FLASH_PATH"

TEST_PATH="$FLASH_PATH/src/src/test"
echo "TEST_PATH: $TEST_PATH"

TOOLS_PATH="$FLASH_PATH/flash-tools"
echo "TOOLS_PATH: $TOOLS_PATH"

FOLDERS=(./scada ./token ./conf)

if [ "$1" = "-jar" ]
then
	cd $TOOLS_PATH
	sbt assembly
	rm -fr $TEST_PATH/lib
  mkdir $TEST_PATH/lib
	mv target/scala-2.12/*.jar $TEST_PATH/lib
	FOLDERS+=(../lib)
fi

cd $TEST_PATH/resources
tar -zcvf flash.tar.gz ${FOLDERS[@]}

cd $ROOT_PATH
mv $TEST_PATH/resources/flash.tar.gz ./
git add .
git commit -m "Flash data updated"
git push